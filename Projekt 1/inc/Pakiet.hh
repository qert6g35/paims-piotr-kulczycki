#ifndef PAKIET_HH
#define PAKIET_HH

#include <iostream>


using namespace std;

class PakietDanych {

private:
	unsigned int ID;	//Nr znaku w tekście
	char Znak; 			// przechowuje literę lub znak z tekstu
	PakietDanych * Nast;  // następny w kolejności (kolejny w stronę ostatniego znaku)
	PakietDanych * Pop;	// poprzedni w kolejności (kolejny w stronę pierwszego znaku)
public:
	
	PakietDanych(char NowyZnak, PakietDanych * NastNowy, unsigned int NowyID);
	PakietDanych(char NowyZnak, unsigned int NowyID);
	PakietDanych();
	void Wysw();
	
	char getZnak();//           Funkcje do Znak
	void setZnak(char NowyZna); 

	unsigned int getID();//		 Funkcje do ID
	void setID(unsigned int NewID);

	PakietDanych* getNast();// Funkcja do Nast
	void setNast(PakietDanych * NowyPakiet);

	PakietDanych* getPop();//  Funkcje do Pop
	void setPop(PakietDanych * NowyPakiet);
};


#endif
