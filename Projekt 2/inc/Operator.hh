#ifndef OPERATOR_HH
#define OPERATOR_HH

#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>     
#include <time.h>

using namespace std;

class Operator {
    private:
        byte *Plansza;
        *GraczB;
        *GraczC;
    public:

        Operator(int iloscDanych);
        Operator(int iloscDanych,int Dane);
        void Wysw();
        int getIloscRek();
        int getOcena(int id);
        void Zamien(int Id1,int Id2);
        string getTytul(int id);
        void ZczytajDane(string NazwaPlikuWiadomos,int IloscRekordow);
        void setArgs(int id,int NowaOcena,string NowyTytul);
        string podmienTytul(int id,string NowyTytul);
        int podmienOcene(int id,int NowaOcena);
       
        //                    SORTOWANIA
        void Sortoj(int jak);

        void SortujScalanie(int LewyKoniec,int PrawyKoniec); // O(n*log(n))
        void Scal(int LewyKoniec,int Srodek,int PrawyKoniec);
        
        int Podziel(int LewyKoniec,int PrawyKoniec);
        void SortujSzybko(int LewyKoniec,int PrawyKoniec);
        
        void SortujKubel();
        //                    FUNKCJIE DODATKOWE

        double WartoscSrednia();
        int Mediana();
        
        
};

#endif
