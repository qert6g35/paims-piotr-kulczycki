#include "Operator.hh"

double Operator::WartoscSrednia(){
	double pom = 0;
	for(int i = 0 ; i<IloscRek ; i++){
		pom +=Oceny[i];
	}
	return pom/IloscRek;
}
int Operator::Mediana(){
	if(IloscRek%2 == 0){
		return((Oceny[IloscRek/2] + Oceny[IloscRek/2 -1] )/2);	
	}else{
		return(Oceny[(IloscRek-1)/2]);
	}
}

Operator::Operator(int iloscDanych){
  Oceny = new int[iloscDanych];
	Tytuly = new string[iloscDanych];
  IloscRek = 0;
}

Operator::Operator(int iloscDanych,int Dane){
  Oceny = new int[iloscDanych];
	Tytuly = new string[iloscDanych];
  IloscRek = iloscDanych;	
}

void Operator::Zamien(int Id1,int Id2){
	int pomOc = Oceny[Id1];
	string pomNaz = Tytuly[Id1];
	Oceny[Id1] = Oceny[Id2];
	Tytuly[Id1] = Tytuly[Id2];
	Oceny[Id2] = pomOc;
	Tytuly[Id2] = pomNaz;
}

int Operator::getIloscRek(){
  return IloscRek;
}

string Operator::podmienTytul(int id,string NowyTytul){
	string pom = Tytuly[id];
	Tytuly[id] = NowyTytul;
	return pom;
}
int Operator::podmienOcene(int id,int NowaOcena){
	int pom = Oceny[id];
	Oceny[id] = NowaOcena;
	return pom;
}

void Operator::setArgs(int id,int NowaOcena,string NowyTytul){
	Tytuly[id] = NowyTytul;
	Oceny[id] = NowaOcena;
}

int Operator::getOcena(int id){
	return Oceny[id];
}

string Operator::getTytul(int id){
	return Tytuly[id];
}

void Operator::Wysw(){
  if(IloscRek == 0){
	  cout<<"\nBrak rekordów w tablicy\n";
  }
  else{
	  for(int i = 0; i<(IloscRek); i++){
		  cout<<"Ocena:"<<Oceny[i]<<" Tytuł:"<<Tytuly[i]<<"\n";
	  }
	}
}

void Operator::Sortoj(int jak){
	switch (jak)
	{
	case 1:
		this->SortujKubel();
		return;
	case 2:
		this->SortujSzybko(0,IloscRek -1);
		return;
	case 3:
		this->SortujScalanie(0,IloscRek -1);
		return;
	default:
		return;
	}
}

void Operator::ZczytajDane(string NazwaPlikuWiadomosc,int IloscRekordow){
	fstream uchwyt;
	uchwyt.open(NazwaPlikuWiadomosc);
	string linia,his1,NowaNazwa;
	getline(uchwyt, linia);
	int licznikP;// Liczniki to pozycje przecinków w lini P - pierwszy K - końcowy
	int licznikK;
	IloscRek = 0;
	while((linia != "")&&(linia!=his1)&&(IloscRek < IloscRekordow)){


	NowaNazwa = "";
	licznikP = 0;
	licznikK = linia.length()-1;
	while(linia[licznikP] != ','){
		licznikP++;
	 }
	 if(linia[linia.length()-1] != ','){
	 	while(linia[licznikK] != ','){
	 		licznikK--;
	 	}
	 	if(linia[licznikK + 2] == '.'){
	 		Oceny[IloscRek] = linia[licznikK + 1 ] - 48;
	 	}
	 	if(linia[licznikK + 3] == '.'){
	 		Oceny[IloscRek] = 10;
	 	}
	 }//zczytano NowaOcena
	 if(Oceny[IloscRek] != 0){
	 	licznikP++;
	 	while(licznikK > licznikP){
	 	NowaNazwa.push_back(linia[licznikP]);
	 	licznikP++;
	 	}
		Tytuly[IloscRek] = NowaNazwa;
		IloscRek++;
	 }//zczytano tytuł
	his1 = linia;
	getline(uchwyt, linia);
	}
	uchwyt.close(); 
}
//                                        quick
int Operator::Podziel(int LewyKoniec,int PrawyKoniec){
  int pivot = Oceny[(LewyKoniec + PrawyKoniec) / 2];
  int i = LewyKoniec; 
  int j = PrawyKoniec; 

    while (true)
    {
        while (Oceny[j] > pivot) j--;

        while (Oceny[i] < pivot) i++;

        if (i < j) 
          Zamien(i++, j--);
        else return j;
    }

}


void Operator::SortujSzybko(int LewyKoniec,int PrawyKoniec){
  if (LewyKoniec < PrawyKoniec)
    {
        int q = Podziel( LewyKoniec, PrawyKoniec);
        SortujSzybko(LewyKoniec, q);
        SortujSzybko(q + 1, PrawyKoniec);
    }
}

//                                          kubelkowe
void Operator::SortujKubel(){
  Operator * tmp[10];
	int ilosc[10];
	int hlp, n;
	n = IloscRek;
    for (int i = 0; i < 10; i++){
      tmp[i] = new Operator(n);
		  ilosc[i]=0;
	}
	
    for(int i = 0; i < n; i++){
		  hlp = Oceny[i]-1;
     	tmp[hlp]->Oceny[ilosc[hlp]] = Oceny[i];
     	tmp[hlp]->Tytuly[ilosc[hlp]] = Tytuly[i];
     	
		  ilosc[hlp]++;
    }
    hlp = 0;
    for(int i = 0; i < 10; i++){
        for (int j = 0; j < ilosc[i]; j++){
            Oceny[hlp] = tmp[i]->Oceny[j];
            Tytuly[hlp] = tmp[i]->Tytuly[j];
            hlp++;
        }
	}
		 
		 
}



//                           Sortowanie przez Scalanie

void Operator::Scal(int LewyKoniec,int Srodek,int PrawyKoniec){
	int lSize = Srodek - LewyKoniec + 1;
	int rSize = PrawyKoniec - Srodek;
	int ILewe = 0;
 	int IPrawe = 0;
 	int i;
	Operator tabL(lSize,1);
	Operator tabR(rSize,1);


	for (int x = 0; x < lSize; x++){
		//tabL.setArgs(x,Oceny[LewyKoniec + x],Tytuly[LewyKoniec + x]); //                  czeeemuuu set agrs nie dziaaałaaa
		
		tabL.Oceny[x] = Oceny[LewyKoniec + x];
		tabL.Tytuly[x] = Tytuly[LewyKoniec + x];
		
		}
		
	for (int y = 0; y < rSize; y++){
		//tabR.setArgs(y,Oceny[Srodek + 1 + y],Tytuly[Srodek + 1 + y]);
		
		tabR.Oceny[y] = Oceny[Srodek + 1 + y];
		tabR.Tytuly[y] = Tytuly[Srodek + 1 + y];
		
		}
 
 	for (i = LewyKoniec; ILewe < lSize && IPrawe < rSize; i++)
 	{
 		if (tabL.getOcena(ILewe) <= tabR.getOcena(IPrawe)){
 			//this->setArgs(i,tabL.getOcena(ILewe),tabL.getTytul(ILewe));
 			
 			Oceny[i] = tabL.Oceny[ILewe];
		  Tytuly[i] = tabL.Tytuly[ILewe];
 			
 			ILewe++;
 		}else{
 			//this->setArgs(i,tabR.getOcena(IPrawe),tabL.getTytul(IPrawe));
 			
 			Oceny[i] = tabR.Oceny[IPrawe];
		  Tytuly[i] = tabR.Tytuly[IPrawe];
		  
 			IPrawe++;
 		}
 	}
 
 	while (ILewe < lSize){
 		//this->setArgs(i,tabL.getOcena(ILewe),tabL.getTytul(ILewe));
 		
 		Oceny[i] = tabL.Oceny[ILewe];
	  Tytuly[i] = tabL.Tytuly[ILewe];
 		
 		i++;
 		ILewe++;
 	}

 	while (IPrawe < rSize){
 		//this->setArgs(i,tabR.getOcena(IPrawe),tabL.getTytul(IPrawe));
 		
 		Oceny[i] = tabR.Oceny[IPrawe];
	  Tytuly[i] = tabR.Tytuly[IPrawe];
	  
 		i++;
 		IPrawe++;
 	}
}
 
 
void Operator::SortujScalanie(int LewyKoniec,int PrawyKoniec){
 	if (PrawyKoniec > LewyKoniec) {
 		int Srodek = (LewyKoniec + PrawyKoniec) / 2;
 		SortujScalanie(LewyKoniec, Srodek);
 		SortujScalanie(Srodek + 1, PrawyKoniec);
 		Scal(LewyKoniec, Srodek, PrawyKoniec);
 	}
}
