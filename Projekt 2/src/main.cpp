#include "Operator.hh"
#include <time.h>

using namespace std;

int main(int argc, char **argv){
  clock_t start;
  clock_t stop;
  int DANESortowania[3][5][2];
  double WartoscSrSortowania[3][5];
  Operator Op(2000000);
  int iloscDanych[4] = {10000,100000,500000,1000000};

  
  ofstream StrumienZapisowy;
  StrumienZapisowy.open("DANESortowaniaProb.txt");

  for(int i = 1; i < 4; i++){
    for(int j = 0 ; j < 4 ; j++ ){
      Op.ZczytajDane("projekt2dane.txt",iloscDanych[j]);
      start = clock();
      Op.Sortoj(i);
      stop = clock();
      cout<<"  Posortowano sposobem"<<i<<" Ilość Danych"<<j<<"\n";
      DANESortowania[i - 1][j][0] = (1000*(stop - start))/(CLOCKS_PER_SEC);
      WartoscSrSortowania[i-1][j] = Op.WartoscSrednia();
      DANESortowania[i - 1][j][1] = Op.Mediana();
      cout<<"Czas: "<<DANESortowania[i - 1][j][0]<<"Wartość śr: "<<WartoscSrSortowania[i-1][j]<<"Mediana: "<<DANESortowania[i - 1][j][1]<<"\n";
      StrumienZapisowy<<DANESortowania[i - 1][j][0]<<","<<WartoscSrSortowania[i-1][j]<<","<<DANESortowania[i - 1][j][1]<<"\n";
    }
  }

  // for(int TypSortowania = 0; TypSortowania < 3; TypSortowania++){
  //   for(int IloscDanych = 0;IloscDanych < 4;IloscDanych++){
  //      StrumienZapisowy<<DANESortowania[TypSortowania][IloscDanych][0]<<","<<WartoscSrSortowania[TypSortowania][IloscDanych]<<","<<DANESortowania[TypSortowania][IloscDanych][1]<<"\n";
  //   }

  // }

  
  StrumienZapisowy.close();
  return 0;
}


//Op.SortujScalanie(0,Op.getIloscRek() - 1); działa ale udajemy że nie sprawdziliśmy nazw 
