#ifndef WARCABY_HH
#define WARCABY_HH

#include <iostream>
#include <fstream>
#include <stdlib.h>     

using namespace std;

const int glebokoscSzukania = 6;

class Ruch{
    public:
    bool CzyRuchZAwansem;
    char Typ;// użwyane w obliczaniu ruchu bota
             // np 'B' bicie 'P' przemieszczenie
    int PozycjaPiona[2];
    int NowaPozycja[2];
};

class MozliweRuchy{
    public:
    bool CzyToSoBicia;
    int IloscRuchow;
    Ruch* Ruchy;

    MozliweRuchy();
    void DodajMozliwyRuch(Ruch DodoawanyRuch);
};


// ____________________________Gracz_________________________________
class Gracz{
    public:
        char KtoGra;
        int IloscPion;
        int IloscDam;
        int PozycjePion[2][20];
        int PozycjeDam[2][20];
    
        Gracz(char Kto);
        int PodajTypPiona(Ruch OtrzymanyRuch);
        void AwansujPiona(int ktory);
        void ZbijFigure(int ktora,int jaka);
        int PodajPozycje(int x,int y);

};
/* ____________________________Gra___________________________________
 * Pozycja[0/1 -> x/y][Piona N]
 *     [0] - >
 *  |   |-1 |   |-1 |   |-1 |   |-1 |   |-1 |0  1 - pion gracza
[1] |-1 |   |-1 |   |-1 |   |-1 |   |-1 |   |1  2 - damka gracza
 |  |   |-1 |   |-1 |   |-1 |   |-1 |   |-1 |2
\ / |-1 |   |-1 |   |-1 |   |-1 |   |-1 |   |3 -1 - pion bota
 V  |   |   |   |   |   |   |   |   |   |   |4 -2 - damka bota 
    |   |   |   |   |   |   |   |   |   |   |5
    |   | 1 |   | 1 |   | 1 |   | 1 |   | 1 |6
    | 1 |   | 1 |   | 1 |   | 1 |   | 1 |   |7
    |   | 1 |   | 1 |   | 1 |   | 1 |   | 1 |8
    | 1 |   | 1 |   | 1 |   | 1 |   | 1 |   |9
      0   1   2   3   4   5   6   7   8   9
*/
class Gra {
    private:
        int Plansza[10][10];
        Gracz * Czlowiek;
        Gracz * Bot;
        char KolorGracza; //           "B" - Biały  "C" - Czarny
    public:
        Gra(char NowyKolorGracza);
        int WykonajRuchGracz(Ruch OtrzymanyRuch);
        int WykonajRuchBot();
        int  AnalizaRuchu(char KtoryGracz,Ruch OtrzymanyRuch,int TypPiona);
        void RozstawPionyNaPlanszy();
        bool CzySaDostBicia(char Kto);
        void Wyswietl();// powinno wyśliwetlc nie tylko plansze ale też kluczowe informacje o rozgrywce
        Ruch ZnajdzNajleprzyRuch(int glembokoscMINIMAX);
        MozliweRuchy ZnajdzMozliweRuchy(char Dla);
        MozliweRuchy ZnajdzMozliweRuchy(char Dla,int* Brudnopis);
        int minimax(Ruch Rozpatrywany,int glembokosc,bool ktoGra,int* Brudnopis,int alpha,int beta);
        void cofnijRuchBrudnopis(Ruch RozpatrywanyRuch,int* Brudnopis,int JakiPionBylZbity);
        int wykonajRuchBrudnopis(Ruch RozpatrywanyRuch,int* Brudnopis);
        int WycenPlansze(int* Brudnopis);
        bool CzyGraczPrzegral();
};




#endif
