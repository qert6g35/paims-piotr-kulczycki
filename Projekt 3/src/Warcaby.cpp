#include "Warcaby.hh"

int BezMinusa(int x){
    if(x < 0) return -x;
    return x;
}

//______________________________________________Gracz
Gracz::Gracz(char Kto){  //                  "B" - Bot
    IloscDam = 0;//                          "G" - Gracz
    IloscPion = 20;
    int NrPionu = 0;
    if(Kto == 'B'){
        for(int y = 0; y<4 ;y++){
            for(int x = 1; x<10 ; x = x + 2){
            PozycjePion[0][NrPionu] = x - y%2;
            PozycjePion[1][NrPionu] = y;
            NrPionu++;    
            KtoGra = 'B';
            }
        }
    }
    if(Kto == 'C'){
        for(int y = 6; y<10 ;y++){
            for(int x = 1; x<10 ; x = x + 2){
            PozycjePion[0][NrPionu] = x - y%2;
            PozycjePion[1][NrPionu] = y;
            NrPionu++;
            KtoGra = 'C';
            }
        }
    }

}

int Gracz::PodajTypPiona(Ruch OtrzymanyRuch){// zwraca 1 lub 2 bez patrzenia na znaki co trzeba uwzględnić
    int x = OtrzymanyRuch.PozycjaPiona[0];
    int y = OtrzymanyRuch.PozycjaPiona[1];
    for(int i = 0; i < IloscPion;i++){
        if((PozycjePion[0][i] ==  x)&&(PozycjePion[1][i] == y)){
            return 1;
        }
    }
        for(int i = 0; i < IloscDam;i++){
        if((PozycjeDam[0][i] ==  x)&&(PozycjeDam[1][i] == y)){
            return 2;
        }
    }
    return 0;
}

void Gracz::AwansujPiona(int ktory){
    if(ktory < IloscPion){
        IloscPion--;
        IloscDam++;
        PozycjeDam[0][IloscDam -1] = PozycjePion[0][ktory];
        PozycjeDam[1][IloscDam -1] = PozycjePion[1][ktory];
        while(ktory < IloscPion){
            PozycjePion[0][ktory] = PozycjePion[0][ktory + 1];
            PozycjePion[1][ktory] = PozycjePion[1][ktory + 1];
            ktory++;
        }
    }else{
        cout<<"\n"<<"Błąd funkcji awansuj Piona"<<"\n";
    }
}


void Gracz::ZbijFigure(int ktora,int jaka){
    if(jaka == 2){// ZBIJANIE DAMY
        if(ktora >= IloscDam){
            cout<<"\n"<<"Błąd zbijania damy (\"ktora\" poza zakresem)"<<"\n";
            return;
        }
        IloscDam--;
        while(ktora < IloscDam){
            PozycjeDam[0][ktora] = PozycjeDam[0][ktora + 1];
            PozycjeDam[1][ktora] = PozycjeDam[1][ktora + 1];
            ktora++;
        }
    }else{// ZBIJANIE PIONA
        if(ktora >= IloscPion){
            cout<<"\n"<<"Błąd zbijania piona (\"ktora\" poza zakresem)"<<"\n";
            return;
        }
        IloscPion--;
        while(ktora < IloscPion){
            PozycjePion[0][ktora] = PozycjePion[0][ktora + 1];
            PozycjePion[1][ktora] = PozycjePion[1][ktora + 1];
            ktora++;
        }
    }
}

int Gracz::PodajPozycje(int x,int y){
    for(int i = 0; i < IloscPion; i++){
        if((PozycjePion[0][i] == x)&&(PozycjePion[1][i] == y)){
            return i;
        }
    }
    for(int i = 0; i < IloscDam; i++){
        if((PozycjeDam[0][i] == x)&&(PozycjeDam[1][i] == y)){
            return i;
        }
    }
    cout<<"\n Nie ma pozycji o takich indexach x i y\n";
    return -1;
}

//______________________________________________MOŻLIWE RUCHY

MozliweRuchy::MozliweRuchy(){
    IloscRuchow = 0;
    Ruchy = new Ruch[0];
}

void MozliweRuchy::DodajMozliwyRuch(Ruch DodoawanyRuch){
    Ruch* Pom = Ruchy;
    IloscRuchow++;
    Ruchy = new Ruch[IloscRuchow];
    for (int i = 0; i < IloscRuchow - 1; i++){
        Ruchy[i] = Pom[i];
    }
    Ruchy[IloscRuchow - 1] = DodoawanyRuch;
    delete[] Pom;
}

//______________________________________________Gra
Gra::Gra(char NowyKolorGracza){
    Czlowiek = new Gracz('C');
    Bot = new Gracz('B');
    KolorGracza = NowyKolorGracza;
}

bool Gra::CzySaDostBicia(char Kto){
    Gracz * Pom;
    int IloscKrokowDamy = 2;
    if(Kto == 'C'){
        Pom = Czlowiek;
    }else{
        Pom = Bot;
    }    
    int Kierunek[4][2] = {
        {1,1},
        {1,-1},
        {-1,-1},
        {-1,1}
    };
    int PionPrzeciwnika = 1;
    int DamaPrzeciwnika = 2;
    if(Kto == 'C'){
        PionPrzeciwnika *= -1;
        DamaPrzeciwnika *= -1;
    }


    for(int i = 0; i<Pom->IloscPion ; i++){
        for(int sprawdzany = 0; sprawdzany <4 ;sprawdzany++){
            if(((Pom->PozycjePion[1][i] + 2* Kierunek[sprawdzany][1] >= 0)&&(Pom->PozycjePion[1][i] + 2* Kierunek[sprawdzany][1] <= 9))&&((Pom->PozycjePion[0][i] + 2* Kierunek[sprawdzany][0] >= 0)&&(Pom->PozycjePion[0][i] + 2* Kierunek[sprawdzany][0] <= 9)))
                if((Plansza[Pom->PozycjePion[1][i] + Kierunek[sprawdzany][1]][Pom->PozycjePion[0][i] + Kierunek[sprawdzany][0]] == PionPrzeciwnika )||(Plansza[Pom->PozycjePion[1][i] + Kierunek[sprawdzany][1]][Pom->PozycjePion[0][i] + Kierunek[sprawdzany][0]] == DamaPrzeciwnika)){
                    if(Plansza[Pom->PozycjePion[1][i] + 2* Kierunek[sprawdzany][1]][Pom->PozycjePion[0][i] + 2* Kierunek[sprawdzany][0]] == 0 ){
                        return 1;
                    }
                }
        }
    }
    for(int i = 0; i<Pom->IloscDam ; i++){
        for(int sprawdzany = 0; sprawdzany <4 ;sprawdzany++){
            while((((Pom->PozycjeDam[1][i] + IloscKrokowDamy* Kierunek[sprawdzany][1]) >= 0)&&(Pom->PozycjeDam[1][i] + IloscKrokowDamy* Kierunek[sprawdzany][1] <= 9))&&((Pom->PozycjeDam[0][i] + IloscKrokowDamy* Kierunek[sprawdzany][0] >= 0)&&(Pom->PozycjeDam[0][i] + IloscKrokowDamy* Kierunek[sprawdzany][0] <= 9))){
                if(Plansza[Pom->PozycjeDam[1][i] + IloscKrokowDamy* Kierunek[sprawdzany][1]][Pom->PozycjeDam[0][i] + IloscKrokowDamy* Kierunek[sprawdzany][0]] == 0){
                    if((Plansza[Pom->PozycjeDam[1][i] + (IloscKrokowDamy - 1)* Kierunek[sprawdzany][1]][Pom->PozycjeDam[0][i] + (IloscKrokowDamy - 1)* Kierunek[sprawdzany][0]] == PionPrzeciwnika)||(Plansza[Pom->PozycjeDam[1][i] + (IloscKrokowDamy - 1)* Kierunek[sprawdzany][1]][Pom->PozycjeDam[0][i] + (IloscKrokowDamy - 1)* Kierunek[sprawdzany][0]] == DamaPrzeciwnika)){
                        return 1;
                    }
                }
                IloscKrokowDamy++;
            }
            IloscKrokowDamy = 2;
        }
    }
    return 0;
}

bool Gra::CzyGraczPrzegral(){
    int x,y;
    int Kierunek[4][2] = {
        {1,-1},
        {-1,-1},
        {1,1},
        {-1,1}
    };
    if((Czlowiek->IloscDam > 0)||(Czlowiek->IloscPion > 0)){
        if(CzySaDostBicia('C')){
            return 0;
        }
        for (int i = 0; i < Czlowiek->IloscPion; i++){
            x = Czlowiek->PozycjePion[0][i];
            y = Czlowiek->PozycjePion[1][i];
            if((Plansza[y + Kierunek[0][1]][x + Kierunek[0][0]] == 0)||(Plansza[y + Kierunek[1][1]][x + Kierunek[1][0]] == 0)){
                return 0;
            }
        }
        for (int i = 0; i < Czlowiek->IloscDam; i++){
            x = Czlowiek->PozycjeDam[0][i];
            y = Czlowiek->PozycjeDam[1][i];
            for (int sprawdzany = 0; sprawdzany < 4; sprawdzany++){
                if(Plansza[y + Kierunek[sprawdzany][1]][x + Kierunek[sprawdzany][0]] == 0){
                    return 0;
                }
            }
            
        }
    }
    return 1;
}


int Gra::WykonajRuchGracz(Ruch OtrzymanyRuch){
    int PozycjaX;
    int PozycjaY;
    Ruch Pom;
    int TypPiona = Czlowiek->PodajTypPiona(OtrzymanyRuch);
    if(TypPiona == 0){
        cout<<"\n"<<"Podano złą pozycję piona ruszanego"<<"\n";
        return 1;
    }
    int TypRuchu = AnalizaRuchu('C',OtrzymanyRuch, TypPiona);
    if(TypRuchu == 0) return 1;

    if((TypRuchu > 0)&&(CzySaDostBicia('C') )){
        cout<<"\n"<<"Próba wykonania zwykłego ruchu gdy dostępne są bicia"<<"\n";
        return 1;
    }
    if(TypRuchu < 0){//  wykonanie bicia
        if(OtrzymanyRuch.NowaPozycja[0] - OtrzymanyRuch.PozycjaPiona[0] < 0){// ustalanie pozycji elementu do zbicia
            PozycjaX = -1 + OtrzymanyRuch.PozycjaPiona[0];
        }else{
            PozycjaX = 1 + OtrzymanyRuch.PozycjaPiona[0];
        }
        if(OtrzymanyRuch.NowaPozycja[1] - OtrzymanyRuch.PozycjaPiona[1] < 0){
            PozycjaY = -1 + OtrzymanyRuch.PozycjaPiona[1];
        }else{
            PozycjaY = 1 + OtrzymanyRuch.PozycjaPiona[1];
        }
        Pom.PozycjaPiona[0] = PozycjaX;
        Pom.PozycjaPiona[1] = PozycjaY;
        Bot->ZbijFigure(Bot->PodajPozycje(PozycjaX,PozycjaY),Bot->PodajTypPiona(Pom));
    }
    // wykonianie ruchu gracza
    PozycjaY = 0;
    PozycjaX = Czlowiek->IloscPion - 1;
    while((PozycjaX >= 0)&&(PozycjaY == 0)){
        if((Czlowiek->PozycjePion[0][PozycjaX] == OtrzymanyRuch.PozycjaPiona[0])&&(Czlowiek->PozycjePion[1][PozycjaX] == OtrzymanyRuch.PozycjaPiona[1])){
            Czlowiek->PozycjePion[0][PozycjaX] = OtrzymanyRuch.NowaPozycja[0];
            Czlowiek->PozycjePion[1][PozycjaX] = OtrzymanyRuch.NowaPozycja[1];
            if(Czlowiek->PozycjePion[1][PozycjaX] == 0){
                Czlowiek->AwansujPiona(PozycjaX);
                cout<<"AWANSOWANO PIONA !!!"<<"\n";
            }
            cout<<"\n"<<"Poprawnie wykonano ruch na x:"<<Czlowiek->PozycjePion[0][PozycjaX]<<" y:"<<Czlowiek->PozycjePion[1][PozycjaX]<<"\n";
            PozycjaY = 1;
        }
        PozycjaX--;
    }
    PozycjaX = Czlowiek->IloscDam - 1;
    while((PozycjaX >= 0)&&(PozycjaY == 0)){
        if((Czlowiek->PozycjeDam[0][PozycjaX] == OtrzymanyRuch.PozycjaPiona[0])&&(Czlowiek->PozycjeDam[1][PozycjaX] == OtrzymanyRuch.PozycjaPiona[1])){
            Czlowiek->PozycjeDam[0][PozycjaX] = OtrzymanyRuch.NowaPozycja[0];
            Czlowiek->PozycjeDam[1][PozycjaX] = OtrzymanyRuch.NowaPozycja[1];
            PozycjaY = 1;
        }
        PozycjaX--;
    }

    return 0;
}

int Gra::AnalizaRuchu(char KtoryGracz,Ruch OtrzymanyRuch,int TypPiona){// zwraca odległość o jaką rusza się pion, następuje bicie gdy zwrócona wartość jest ujemna
    if(Plansza[OtrzymanyRuch.NowaPozycja[1]][OtrzymanyRuch.NowaPozycja[0]] != 0){
        cout<<"\n"<<"BŁĄD RUCHU, próba stanięcia na innym pionie/damce"<<"\n";
        return 0;
    }

    int RuchX = OtrzymanyRuch.NowaPozycja[0] - OtrzymanyRuch.PozycjaPiona[0];
    int RuchY = OtrzymanyRuch.NowaPozycja[1] - OtrzymanyRuch.PozycjaPiona[1];
    
    //cout<<"\n"<<RuchX<<" "<<RuchY<<" "<<TypPiona<<"\n";

    if((RuchX == 0)&&(RuchY == 0)){
        cout<<"\n"<<"BŁĄD RUCHU, wykonano ruch o \"0\""<<"\n";
        return 0;
    }

    if(BezMinusa(RuchX) != BezMinusa(RuchY)){
        cout<<"\n"<<"BŁĄD RUCHU, wykonano ruch nie po diagonali"<<"\n";
        return 0;
    }
    if((TypPiona == 1)||(TypPiona == -1)){// Ruch zwykłego piona
        if(BezMinusa(RuchX) == 2){
            if( Plansza[OtrzymanyRuch.PozycjaPiona[1] + RuchY/2][OtrzymanyRuch.PozycjaPiona[0]+ RuchX/2] == 0){
               cout<<"\n"<<"BŁĄD RUCHU, próba bicia pustego pola"<<"\n";
               return 0;
            }else{
               return -BezMinusa(RuchX);
            }
        }
        if(RuchY > 0){
            cout<<"\n"<<"BŁĄD RUCHU, próba ruszenia zwykłym pionę w złą stronę"<<"\n";
            return 0;
        }
        if(BezMinusa(RuchX) != 1){
            cout<<"\n"<<"BŁĄD RUCHU, próba ruszenia zwykłym pionę o złą ilość kratek na raz"<<"\n";
            return 0;
        }
        return BezMinusa(RuchX);
    }
    if((TypPiona == 2)||(TypPiona == -2)){// Ruch Damki
        int PomocnikPozycji[2];
        int KierunekRuchu[2];
        if(RuchX < 0){//                wyznaczanie kierunków ruchu 
            KierunekRuchu[0] = -1;
        }else{
            KierunekRuchu[0] = 1;
        }
        if(RuchY < 0){
            KierunekRuchu[1] = -1;
        }else{
            KierunekRuchu[1] = 1;
        }
        PomocnikPozycji[0] = OtrzymanyRuch.PozycjaPiona[0];
        PomocnikPozycji[1] = OtrzymanyRuch.PozycjaPiona[1];
        //                                               analiza ruchu damką
        for(int i = 0; i < BezMinusa(RuchX) - 2; i++){
            PomocnikPozycji[0] += KierunekRuchu[0];
            PomocnikPozycji[1] += KierunekRuchu[1];
            if(Plansza[PomocnikPozycji[1]][PomocnikPozycji[0]] != 0){
                cout<<"\n"<<"BŁĄD RUCHU, próba bicia damką więcej niż 1 piona na raz\nLub pruba przelotu damką nad pionem"<<"\n";
                return 0;
            }
        }
        PomocnikPozycji[0] += KierunekRuchu[0];
        PomocnikPozycji[1] += KierunekRuchu[1];
        if(Plansza[PomocnikPozycji[1]][PomocnikPozycji[0]] == 0){
            return BezMinusa(RuchX);
        }else{
            if(Plansza[PomocnikPozycji[1]][PomocnikPozycji[0]] < 0){
                return -BezMinusa(RuchX);
            }else{
                cout<<"\n"<<"BŁĄD RUCHU, próba bicia damką własnego piona"<<"\n";
                return 0;
            }
        }
    }
    cout<<"\n"<<"BŁĄD RUCHU, Dotarto do końca analizy ruchu"<<"\n";
    return 0;
}

MozliweRuchy Gra::ZnajdzMozliweRuchy(char Dla){
    MozliweRuchy ListaRuchow;
    ListaRuchow.CzyToSoBicia = 0;
    Ruch PomListy;
    PomListy.CzyRuchZAwansem = 0;
    bool FlagaKoncaZasiengu = 1;
    int x,y;
    int KoniecPlanszy = 9;
    int Kto = 1;
    if(Dla == 'C'){
        Kto = -1;
        KoniecPlanszy = 0;
    }
    int Kierunek[4][2] = {
    {1,1},
    {-1,1},
    {-1,-1},
    {1,-1}
    };


    if(CzySaDostBicia(Dla)){//________________________znajdź wszystkie dostępne bicia__________CzySaDostBicia('B')
        ListaRuchow.CzyToSoBicia = 1;
        PomListy.Typ = 'B';
        for (int KtoraDama = 0; KtoraDama < Bot->IloscDam; KtoraDama++){// szukanie bić damek
            for (int sprawdzany = 0; sprawdzany < 4; sprawdzany++){
                x = Bot->PozycjeDam[0][KtoraDama] + Kierunek[sprawdzany][0];
                y = Bot->PozycjeDam[1][KtoraDama] + Kierunek[sprawdzany][1];
                while(((y>=0)&&(y<=9))&&((x>=0)&&(x<=9))&&FlagaKoncaZasiengu){
                    if(Plansza[y][x] == 0){
                        x += Kierunek[sprawdzany][0];
                        y += Kierunek[sprawdzany][1];
                    }else{
                        FlagaKoncaZasiengu = 0;
                        if((Plansza[y][x] == 1 * Kto)||(Plansza[y][x] == 2 * Kto)){
                            x += Kierunek[sprawdzany][0];
                            y += Kierunek[sprawdzany][1];  
                            if(((y>=0)&&(y<=9))&&((x>=0)&&(x<=9))){
                                if(Plansza[y][x] == 0){
                                    PomListy.PozycjaPiona[0] = Bot->PozycjeDam[0][KtoraDama];
                                    PomListy.PozycjaPiona[1] = Bot->PozycjeDam[1][KtoraDama];
                                    PomListy.NowaPozycja[0] = x;
                                    PomListy.NowaPozycja[1] = y;
                                    ListaRuchow.DodajMozliwyRuch(PomListy);
                                }
                            }
                        }
                    }
                }
                FlagaKoncaZasiengu = 1;
            }
        }
        for (int KtoryPion = 0; KtoryPion < Bot->IloscPion; KtoryPion++){// szukanie bić pionów
            for (int sprawdzany = 0; sprawdzany < 4; sprawdzany++){
                x = Bot->PozycjePion[0][KtoryPion] + Kierunek[sprawdzany][0];
                y = Bot->PozycjePion[1][KtoryPion] + Kierunek[sprawdzany][1];
                if(((y>=0)&&(y<=9))&&((x>=0)&&(x<=9))){
                    if((Plansza[y][x] == 1 * Kto)||(Plansza[y][x] == 2 * Kto)){
                        x += Kierunek[sprawdzany][0];
                        y += Kierunek[sprawdzany][1]; 
                        if(((y>=0)&&(y<=9))&&((x>=0)&&(x<=9))){
                            if(Plansza[y][x] == 0){
                                PomListy.PozycjaPiona[0] = Bot->PozycjePion[0][KtoryPion];
                                PomListy.PozycjaPiona[1] = Bot->PozycjePion[1][KtoryPion];
                                PomListy.NowaPozycja[0] = x;
                                PomListy.NowaPozycja[1] = y;
                                if(y == KoniecPlanszy){
                                    PomListy.CzyRuchZAwansem = 1;
                                }
                                ListaRuchow.DodajMozliwyRuch(PomListy);   
                                //cout<<"\n Znaleziono i dodano bicie na x:"<<PomListy.NowaPozycja[0]<<" y:"<<PomListy.NowaPozycja[1]<<"\n";
                                PomListy.CzyRuchZAwansem = 0;
                            }
                        }
                    }
                }
            }
        }
        
        
    }else{//__________________________________________znajdź wszystkie dostępne ruchy
        PomListy.Typ = 'P';
        for(int i = 0; i < Bot->IloscDam ; i++){//  RUCHY DAMAMI
            for (int sprawdzany = 0; sprawdzany < 4; sprawdzany++){
                y = Bot->PozycjeDam[1][i] + Kierunek[sprawdzany][1];
                x = Bot->PozycjeDam[0][i] + Kierunek[sprawdzany][0];
                while(((y>=0)&&(y<=9))&&((x>=0)&&(x<=9))&&(FlagaKoncaZasiengu)){
                    if(Plansza[y][x] == 0){
                        PomListy.PozycjaPiona[0] = Bot->PozycjeDam[0][i];
                        PomListy.PozycjaPiona[1] = Bot->PozycjeDam[1][i];
                        PomListy.NowaPozycja[0] = x;
                        PomListy.NowaPozycja[1] = y;
                        ListaRuchow.DodajMozliwyRuch(PomListy);
                    }else{
                        FlagaKoncaZasiengu = 0;
                    }
                    y += Kierunek[sprawdzany][1];
                    x += Kierunek[sprawdzany][0];
                }
                FlagaKoncaZasiengu = 1;
            }
        }
        for(int i = 0; i < Bot->IloscPion; i++){//  RUCHY PIONAMI
            int sprawdzany = 0;
            int pom = 2;
            if(Dla == 'C'){
                sprawdzany = 2;
                pom = 4;
            }
            while( sprawdzany < pom){
                y = Bot->PozycjePion[1][i] + Kierunek[sprawdzany][1];
                x = Bot->PozycjePion[0][i] + Kierunek[sprawdzany][0];
                if(((y>=0)&&(y<=9))&&((x>=0)&&(x<=9))){
                    if(Plansza[y][x] == 0){
                        PomListy.PozycjaPiona[0] = Bot->PozycjePion[0][i];
                        PomListy.PozycjaPiona[1] = Bot->PozycjePion[1][i];
                        PomListy.NowaPozycja[0] = x;
                        PomListy.NowaPozycja[1] = y;
                        if(y == KoniecPlanszy){
                            PomListy.CzyRuchZAwansem = 1;
                        }
                        ListaRuchow.DodajMozliwyRuch(PomListy);
                        PomListy.CzyRuchZAwansem = 0;
                    }
                }
                sprawdzany++;
            }
        }
    }
    return ListaRuchow;
}

MozliweRuchy Gra::ZnajdzMozliweRuchy(char Dla,int* Brudnopis){//TODO 
    MozliweRuchy ListaRuchow;
    ListaRuchow.CzyToSoBicia = 0;
    Ruch PomListy;
    PomListy.CzyRuchZAwansem = 0;
    bool FlagaKoncaZasiengu = 1;
    int x,y;
    int KoniecPlanszy = 9;
    int Kto = 1;
    int PozycjeDam[2][20],PozycjePion[2][20];
    int IloscDam = 0,IloscPion = 0;
    if(Dla == 'C'){
        Kto = -1;
        KoniecPlanszy = 0;
    }
    int Kierunek[4][2] = {
    {1,1},
    {-1,1},
    {-1,-1},
    {1,-1}
    };

    for(int i = 0;i<10;i++){
        for (int j = 0; j < 10; j++)
        {
            if(Brudnopis[i*10+j] == -Kto*1){// znaleziono piona
                IloscPion++;
                PozycjePion[0][IloscPion -1] = j;
                PozycjePion[1][IloscPion -1] = i;
            }
            if(Brudnopis[i*10+j] == -Kto*2){// znaleziono damke
                IloscDam++;
                PozycjeDam[0][IloscPion -1] = j;
                PozycjeDam[1][IloscPion -1] = i;
            }
        }
    }
    

    if(CzySaDostBicia(Dla)){//________________________znajdź wszystkie dostępne bicia__________CzySaDostBicia('B')
        ListaRuchow.CzyToSoBicia = 1;
        PomListy.Typ = 'B';
        for (int KtoraDama = 0; KtoraDama < IloscDam; KtoraDama++){// szukanie bić damek
            for (int sprawdzany = 0; sprawdzany < 4; sprawdzany++){
                x = PozycjeDam[0][KtoraDama] + Kierunek[sprawdzany][0];
                y = PozycjeDam[1][KtoraDama] + Kierunek[sprawdzany][1];
                while(((y>=0)&&(y<=9))&&((x>=0)&&(x<=9))&&FlagaKoncaZasiengu){
                    if(Brudnopis[y*10 + x] == 0){
                        x += Kierunek[sprawdzany][0];
                        y += Kierunek[sprawdzany][1];
                    }else{
                        FlagaKoncaZasiengu = 0;
                        if((Brudnopis[y*10 + x] == 1 * Kto)||(Brudnopis[y*10 + x] == 2 * Kto)){
                            x += Kierunek[sprawdzany][0];
                            y += Kierunek[sprawdzany][1];  
                            if(((y>=0)&&(y<=9))&&((x>=0)&&(x<=9))){
                                if(Brudnopis[y*10 + x] == 0){
                                    PomListy.PozycjaPiona[0] = PozycjeDam[0][KtoraDama];
                                    PomListy.PozycjaPiona[1] = PozycjeDam[1][KtoraDama];
                                    PomListy.NowaPozycja[0] = x;
                                    PomListy.NowaPozycja[1] = y;
                                    ListaRuchow.DodajMozliwyRuch(PomListy);
                                }
                            }
                        }
                    }
                }
                FlagaKoncaZasiengu = 1;
            }
        }
        for (int KtoryPion = 0; KtoryPion < IloscPion; KtoryPion++){// szukanie bić pionów
            for (int sprawdzany = 0; sprawdzany < 4; sprawdzany++){
                x = PozycjePion[0][KtoryPion] + Kierunek[sprawdzany][0];
                y = PozycjePion[1][KtoryPion] + Kierunek[sprawdzany][1];
                if(((y>=0)&&(y<=9))&&((x>=0)&&(x<=9))){
                    if((Brudnopis[y*10 + x] == 1 * Kto)||(Brudnopis[y*10 + x] == 2 * Kto)){
                        x += Kierunek[sprawdzany][0];
                        y += Kierunek[sprawdzany][1]; 
                        if(((y>=0)&&(y<=9))&&((x>=0)&&(x<=9))){
                            if(Brudnopis[y*10 + x] == 0){
                                PomListy.PozycjaPiona[0] = PozycjePion[0][KtoryPion];
                                PomListy.PozycjaPiona[1] = PozycjePion[1][KtoryPion];
                                PomListy.NowaPozycja[0] = x;
                                PomListy.NowaPozycja[1] = y;
                                if(y == KoniecPlanszy){
                                    PomListy.CzyRuchZAwansem = 1;
                                }
                                ListaRuchow.DodajMozliwyRuch(PomListy);   
                                //cout<<"\n Znaleziono i dodano bicie na x:"<<PomListy.NowaPozycja[0]<<" y:"<<PomListy.NowaPozycja[1]<<"\n";
                                PomListy.CzyRuchZAwansem = 0;
                            }
                        }
                    }
                }
            }
        }
        
        
    }else{//__________________________________________znajdź wszystkie dostępne ruchy
        PomListy.Typ = 'P';
        for(int i = 0; i < IloscDam ; i++){//  RUCHY DAMAMI
            for (int sprawdzany = 0; sprawdzany < 4; sprawdzany++){
                y = PozycjeDam[1][i] + Kierunek[sprawdzany][1];
                x = PozycjeDam[0][i] + Kierunek[sprawdzany][0];
                while(((y>=0)&&(y<=9))&&((x>=0)&&(x<=9))&&(FlagaKoncaZasiengu)){
                    if(Brudnopis[y*10 + x] == 0){
                        PomListy.PozycjaPiona[0] = PozycjeDam[0][i];
                        PomListy.PozycjaPiona[1] = PozycjeDam[1][i];
                        PomListy.NowaPozycja[0] = x;
                        PomListy.NowaPozycja[1] = y;
                        ListaRuchow.DodajMozliwyRuch(PomListy);
                    }else{
                        FlagaKoncaZasiengu = 0;
                    }
                    y += Kierunek[sprawdzany][1];
                    x += Kierunek[sprawdzany][0];
                }
                FlagaKoncaZasiengu = 1;
            }
        }
        for(int i = 0; i < IloscPion; i++){//  RUCHY PIONAMI
            int sprawdzany = 0;
            int pom = 2;
            if(Dla == 'C'){
                sprawdzany = 2;
                pom = 4;
            }
            while( sprawdzany < pom){
                y = PozycjePion[1][i] + Kierunek[sprawdzany][1];
                x = PozycjePion[0][i] + Kierunek[sprawdzany][0];
                if(((y>=0)&&(y<=9))&&((x>=0)&&(x<=9))){
                    if(Brudnopis[y*10 + x] == 0){
                        PomListy.PozycjaPiona[0] = PozycjePion[0][i];
                        PomListy.PozycjaPiona[1] = PozycjePion[1][i];
                        PomListy.NowaPozycja[0] = x;
                        PomListy.NowaPozycja[1] = y;
                        if(y == KoniecPlanszy){
                            PomListy.CzyRuchZAwansem = 1;
                        }
                        ListaRuchow.DodajMozliwyRuch(PomListy);
                        PomListy.CzyRuchZAwansem = 0;
                    }
                }
                sprawdzany++;
            }
        }
    }
    return ListaRuchow;
}

void Gra::cofnijRuchBrudnopis(Ruch RozpatrywanyRuch,int* Brudnopis,int JakiPionBylZbity){//TODO dodaj cofanie awansu piona jeżeli takie nastąpiło
    int Kierunek[2];
    Kierunek[0] = 1;
    Kierunek[1] = 1;
    int RodzajPiona;
    if(RozpatrywanyRuch.PozycjaPiona[0] - RozpatrywanyRuch.NowaPozycja[0] < 0){
        Kierunek[0] = -1;
    }
    if(RozpatrywanyRuch.PozycjaPiona[1] - RozpatrywanyRuch.NowaPozycja[1] < 0){
        Kierunek[1] = -1;
    }
    if(RozpatrywanyRuch.CzyRuchZAwansem){
        RodzajPiona = Brudnopis[RozpatrywanyRuch.NowaPozycja[1]*10 + RozpatrywanyRuch.NowaPozycja[0]];
        if(RodzajPiona < 0){
            RodzajPiona = -1;
        }else{
            RodzajPiona = 1;
        }
    }else{
        RodzajPiona = Brudnopis[RozpatrywanyRuch.NowaPozycja[1]*10 + RozpatrywanyRuch.NowaPozycja[0]];
    }
    Brudnopis[RozpatrywanyRuch.PozycjaPiona[1]*10 + RozpatrywanyRuch.PozycjaPiona[0]] = RodzajPiona;
    Brudnopis[RozpatrywanyRuch.NowaPozycja[1]*10 + RozpatrywanyRuch.NowaPozycja[0]] = 0;
    if(JakiPionBylZbity != 0)
    Brudnopis[(RozpatrywanyRuch.NowaPozycja[1] + Kierunek[1])*10 + RozpatrywanyRuch.NowaPozycja[0] + Kierunek[0]] = JakiPionBylZbity;
}

int Gra::wykonajRuchBrudnopis(Ruch RozpatrywanyRuch,int* Brudnopis){
    int RodzajPiona[2];
    int Kierunek[2];
    Kierunek[0] = 1;
    Kierunek[1] = 1;
    if(RozpatrywanyRuch.PozycjaPiona[0] - RozpatrywanyRuch.NowaPozycja[0] < 0){
        Kierunek[0] = -1;
    }
    if(RozpatrywanyRuch.PozycjaPiona[1] - RozpatrywanyRuch.NowaPozycja[1] < 0){
        Kierunek[1] = -1;
    }

    if(RozpatrywanyRuch.Typ == 'B'){// wykonanie bicia na brudnopisie
        RodzajPiona[0] = Brudnopis[RozpatrywanyRuch.PozycjaPiona[1]*10 + RozpatrywanyRuch.PozycjaPiona[0]];
        RodzajPiona[1] =  Brudnopis[(RozpatrywanyRuch.NowaPozycja[1] + Kierunek[1])*10 + RozpatrywanyRuch.NowaPozycja[0] + Kierunek[0]];
        if(RozpatrywanyRuch.CzyRuchZAwansem){
            Brudnopis[RozpatrywanyRuch.PozycjaPiona[1]*10 + RozpatrywanyRuch.PozycjaPiona[0]] = 0;
            if(RodzajPiona[0] > 0){
                Brudnopis[RozpatrywanyRuch.NowaPozycja[1]*10 + RozpatrywanyRuch.NowaPozycja[0]] = 2;}
            else{
                Brudnopis[RozpatrywanyRuch.NowaPozycja[1]*10 + RozpatrywanyRuch.NowaPozycja[0]] = -2;}
            Brudnopis[(RozpatrywanyRuch.NowaPozycja[1] + Kierunek[1])*10 + RozpatrywanyRuch.NowaPozycja[0] + Kierunek[0]] = 0;
        }else{
            Brudnopis[RozpatrywanyRuch.PozycjaPiona[1]*10 + RozpatrywanyRuch.PozycjaPiona[0]] = 0;
            Brudnopis[RozpatrywanyRuch.NowaPozycja[1]*10 + RozpatrywanyRuch.NowaPozycja[0]] = RodzajPiona[0];
            Brudnopis[(RozpatrywanyRuch.NowaPozycja[1] + Kierunek[1])*10 + RozpatrywanyRuch.NowaPozycja[0] + Kierunek[0]] = 0;
        }
        return RodzajPiona[1];
    }else{// wykonanie ruchu na brudnopisie
        RodzajPiona[0] = Brudnopis[(RozpatrywanyRuch.PozycjaPiona[1])*10 + RozpatrywanyRuch.PozycjaPiona[0]];
        Brudnopis[RozpatrywanyRuch.PozycjaPiona[1]*10 + RozpatrywanyRuch.PozycjaPiona[0]] = 0;
        if(RozpatrywanyRuch.CzyRuchZAwansem){
            if(RodzajPiona[0] > 0){
                Brudnopis[RozpatrywanyRuch.NowaPozycja[1]*10 + RozpatrywanyRuch.NowaPozycja[0]] = 2;}
            else{
                Brudnopis[RozpatrywanyRuch.NowaPozycja[1]*10 + RozpatrywanyRuch.NowaPozycja[0]] = -2;}
        }
        return 0;
    }
    
}

int Gra::WycenPlansze(int* Brudnopis){// każda przewaga bota +, gracza -;
    int WartoscPlanszy = 0;
    for (int i = 0; i < 100; i++)
    {
        if(Brudnopis[i] == -1) WartoscPlanszy +=2;
        if(Brudnopis[i] == -2) WartoscPlanszy +=5;
        if(Brudnopis[i] == 1) WartoscPlanszy -=2;
        if(Brudnopis[i] == 2) WartoscPlanszy -=5;
    }
    
    return WartoscPlanszy;
}

int Gra::minimax(Ruch Rozpatrywany,int glembokosc,bool ktoGra,int* Brudnopis,int alpha,int beta){
    int CoZbito = wykonajRuchBrudnopis(Rozpatrywany,Brudnopis);
    char Kto;
    if(ktoGra){ 
        Kto = 'B';
    }else{
        Kto = 'C';
    }
    // Mozliwosci przechowuje wsztykie możliwe ruchy lub jeżeli są dostępne bici to wszystkie możliwe bicia 
    MozliweRuchy Mozliwosci = ZnajdzMozliweRuchy(Kto,Brudnopis);

    if(Mozliwosci.IloscRuchow <= 0){//___Sprawdzanie czy koniec gry, gra się kończy gdy nie masz już żadnego ruchu do wykonania
        if(ktoGra){
            cofnijRuchBrudnopis(Rozpatrywany,Brudnopis,CoZbito);
            return 101;
        }else{
            cofnijRuchBrudnopis(Rozpatrywany,Brudnopis,CoZbito);
            return -101;
        }
    }

    if(glembokosc == 0){
        int WartoscRuchu = WycenPlansze(Brudnopis);
        cofnijRuchBrudnopis(Rozpatrywany,Brudnopis,CoZbito);
        return WartoscRuchu;
    }
    int OcenaRuchow;
    int NajleprzaOcena;


    if(ktoGra){// maksymalizacja ruchu bota
        NajleprzaOcena = -1000;
        for (int i = 0; i < Mozliwosci.IloscRuchow; i++)
        {
            OcenaRuchow = minimax(Mozliwosci.Ruchy[i],glembokosc - 1,0,Brudnopis,alpha,beta);
            if(NajleprzaOcena < OcenaRuchow){
                NajleprzaOcena = OcenaRuchow;
            }
            if(alpha < OcenaRuchow){
                alpha = OcenaRuchow;
            }
            if(beta <= alpha) break;    
        }
    }else{// minimalizacja ruchu bota
        NajleprzaOcena = 1000;
        for (int i = 0; i < Mozliwosci.IloscRuchow; i++)
        {
            OcenaRuchow = minimax(Mozliwosci.Ruchy[i],glembokosc - 1,1,Brudnopis,alpha,beta);
            if(NajleprzaOcena > OcenaRuchow){
                NajleprzaOcena = OcenaRuchow;
            }
            if(beta > OcenaRuchow){
                beta = OcenaRuchow;
            }
            if(beta <= alpha) break;
        }
    }
    cofnijRuchBrudnopis(Rozpatrywany,Brudnopis,CoZbito);
    return NajleprzaOcena;
}

Ruch Gra::ZnajdzNajleprzyRuch(int glembokoscMINIMAX){
    MozliweRuchy Ruchy = ZnajdzMozliweRuchy('B');
    int* Brudnopis;
    Ruch najleprzy;
    int* OcenaRuchow = new int[Ruchy.IloscRuchow];
    Brudnopis = new int[100];
    if(Ruchy.IloscRuchow == 0){
        najleprzy.PozycjaPiona[0] = 0;
        najleprzy.PozycjaPiona[1] = 0;
        najleprzy.NowaPozycja[0] = 0;
        najleprzy.NowaPozycja[1] = 0;
        return najleprzy;
    }
    RozstawPionyNaPlanszy();
    for(int i = 0;i<10;i++){
        for(int j = 0;j<10;j++){
            Brudnopis[i*10 + j] = Plansza[i][j];
        }   
    }

    //cout<<"\n Znaleziono :"<<Ruchy.IloscRuchow<<" I są to [0-ruchy][1-bicia]:"<<Ruchy.CzyToSoBicia<<"\n";
    for(int i = 0; i<Ruchy.IloscRuchow; i++){
        OcenaRuchow[i] = minimax(Ruchy.Ruchy[i],glembokoscMINIMAX,1,Brudnopis,-9999,9999);  
    }
    int IdNajleprzegoRuchu = 0;
    for (int i = 0; i < Ruchy.IloscRuchow; i++){
        //cout<<"\n Ruch nr:"<<i<<" o ocenie:"<<OcenaRuchow[i]<<"ruch to x:"<<Ruchy.Ruchy[i].PozycjaPiona[0]<<" i y:"<<Ruchy.Ruchy[i].PozycjaPiona[1]<<"\n Na x:"<<Ruchy.Ruchy[i].NowaPozycja[0]<<" y;"<<Ruchy.Ruchy[i].NowaPozycja[1]<<"\n";
        if(OcenaRuchow[IdNajleprzegoRuchu] < OcenaRuchow[i]){
            IdNajleprzegoRuchu = i;
        }
    }
    najleprzy = Ruchy.Ruchy[IdNajleprzegoRuchu];
    
    delete[] Brudnopis;
    delete[] OcenaRuchow;
    return najleprzy;
}

int Gra::WykonajRuchBot(){
    Ruch NajleprzyZnaleziony = ZnajdzNajleprzyRuch(glebokoscSzukania);///
    //cout<<"\n Wg. Bota najleprzy ruch to x:"<<NajleprzyZnaleziony.PozycjaPiona[0]<<" i y:"<<NajleprzyZnaleziony.PozycjaPiona[1]<<"\n Na x:"<<NajleprzyZnaleziony.NowaPozycja[0]<<" y;"<<NajleprzyZnaleziony.NowaPozycja[1]<<"\n";
    if((NajleprzyZnaleziony.PozycjaPiona[0] == 0)&&(NajleprzyZnaleziony.PozycjaPiona[1] == 0)){
        return 2;
    }
    cout<<"\n Bot wykonuje Ruch z pola  x:"<<NajleprzyZnaleziony.PozycjaPiona[0]<<" y:"<<NajleprzyZnaleziony.PozycjaPiona[1]<<", na pole  x:"<<NajleprzyZnaleziony.NowaPozycja[0]<<" y:"<<NajleprzyZnaleziony.NowaPozycja[1]<<"\n";
    // Trzeba wykonać 3 rzeczy 
    //1 ustawić ruszanego piona na nowe miejsce 
    //2 jeżeli to bicie zbić piona o konkretnym połorzeniu
    //3 jeżeli to awans to awansować konkretnego piona
    int ID = -1;
    //1
    for (int i = 0; i < Bot->IloscDam; i++){
        if((NajleprzyZnaleziony.PozycjaPiona[0] == Bot->PozycjeDam[0][i])&&(NajleprzyZnaleziony.PozycjaPiona[1] == Bot->PozycjeDam[1][i])){
            Bot->PozycjeDam[0][i] = NajleprzyZnaleziony.NowaPozycja[0];
            Bot->PozycjeDam[1][i] = NajleprzyZnaleziony.NowaPozycja[1];
        }
    }
    for (int i = 0; i < Bot->IloscPion; i++){
        if((NajleprzyZnaleziony.PozycjaPiona[0] == Bot->PozycjePion[0][i])&&(NajleprzyZnaleziony.PozycjaPiona[1] == Bot->PozycjePion[1][i])){
            Bot->PozycjePion[0][i] = NajleprzyZnaleziony.NowaPozycja[0];
            Bot->PozycjePion[1][i] = NajleprzyZnaleziony.NowaPozycja[1];
            ID = i;
        }
    }
    //3
    if(NajleprzyZnaleziony.CzyRuchZAwansem){
        Bot->AwansujPiona(ID);
    }
    //2
    if(NajleprzyZnaleziony.Typ == 'B'){
        int Kierunek[2];
        
        Kierunek[0] = 1;
        Kierunek[1] = 1;
        if(NajleprzyZnaleziony.PozycjaPiona[0] - NajleprzyZnaleziony.NowaPozycja[0] < 0){
            Kierunek[0] = -1;
        }
        if(NajleprzyZnaleziony.PozycjaPiona[1] - NajleprzyZnaleziony.NowaPozycja[1] < 0){
            Kierunek[1] = -1;
        }
        int ZbijaneX = NajleprzyZnaleziony.NowaPozycja[0] + Kierunek[0];
        int ZbijaneY = NajleprzyZnaleziony.NowaPozycja[1] + Kierunek[1];


        for (int i = 0; i < Czlowiek->IloscPion; i++){
            if((Czlowiek->PozycjePion[0][i] == ZbijaneX)&&(Czlowiek->PozycjePion[1][i] == ZbijaneY)){
                Czlowiek->ZbijFigure(i,1);
            }
        }
        for (int i = 0; i < Czlowiek->IloscDam; i++){
            if((Czlowiek->PozycjeDam[0][i] == ZbijaneX)&&(Czlowiek->PozycjeDam[1][i] == ZbijaneY)){
                Czlowiek->ZbijFigure(i,2);
            }
        }

    }
    
    
    return 0;
}

void Gra::RozstawPionyNaPlanszy(){
    for (int i = 0; i < 10; i++)
    {
        for (int j = 0; j < 10; j++)
        {
            Plansza[i][j] = 0;
        }
        
    }
    
    for(int i = 0;i < Czlowiek->IloscPion;i ++){
        Plansza[Czlowiek->PozycjePion[1][i]][Czlowiek->PozycjePion[0][i]] = 1;
    }
    for(int i = 0;i < Czlowiek->IloscDam;i ++){
        Plansza[Czlowiek->PozycjeDam[1][i]][Czlowiek->PozycjeDam[0][i]] = 2;
    }
    for(int i = 0;i < Bot->IloscPion;i ++){
        Plansza[Bot->PozycjePion[1][i]][Bot->PozycjePion[0][i]] = -1;
    }
    for(int i = 0;i < Bot->IloscDam;i ++){
        Plansza[Bot->PozycjeDam[1][i]][Bot->PozycjeDam[0][i]] = -2;
    }
}

void Gra::Wyswietl(){
    RozstawPionyNaPlanszy();
    cout<<"__0___1___2___3___4___5___6___7___8___9__\n";
    for(int y = 0; y < 10; y++){
        for (int x = 0; x < 10; x++)
        {
            if(Plansza[y][x] == 0){
                cout<<"| "<<" "<<" ";
            }else{
                if(Plansza[y][x] > 0){
                    cout<<"| "<<Plansza[y][x]<<" ";
                }else{
                    cout<<"|"<<Plansza[y][x]<<" ";
                }
            }
        }
    cout<<"|"<<y<<"\n";
    }
    cout<<"\n";
}